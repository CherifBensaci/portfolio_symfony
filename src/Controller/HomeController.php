<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Porfolio;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'Accueil',
        ]);

    }

    #[Route('/porfolio', name: 'porfolio')]
    public function porfolio(ManagerRegistry $doctrine): Response
    {
        $portfolio = $doctrine->getRepository(Porfolio::class)->findAll();

        return $this->render('home/porfolio.html.twig', [
            'controller_name' => 'Porfolio',
            'data' => $portfolio,
        ]);

    }


}
